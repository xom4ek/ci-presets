def call(props, name, build_options='') {
  base = "${name}:${props.branch_tag}"
  cache_from = "--cache-from ${base}"

  if (props.multistage == true) {
    sh "docker pull     ${base}-builder || true"
    sh "docker build -t ${base}-builder \
        --target builder \
        --cache-from ${base}-builder ${build_options} ."
    sh "docker push  ${base}-builder"
    cache_from = "--cache-from ${base}-builder ${cache_from}"
  }

  sh "docker pull ${base} || true"
  sh "docker build \
    -t ${name}:${props.branch_tag} \
    -t ${name}:${props.permanent_tag} \
    ${cache_from} ${build_options} ."
  sh "docker push ${name}:${props.branch_tag}"
  sh "docker push ${name}:${props.permanent_tag}"
}
